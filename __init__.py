aminoacids = "ACDEFGHIKLMNOPQRSTUVWY"

class Protein:

            def __init__(self, id, name, os, ox, gn, pe, sv, sequence):
                self.id = id
                self.name = name
                self.os = os
                self.ox = ox
                self.gn = gn
                self.pe = pe
                self.sv = sv
                self.sequence = sequence
                self.size = len(sequence)
                aminoacidsdict = {}
             
                for element in aminoacids:
                   aminoacidsdict[element] = sequence.count(element)
                self.aminoacidcounts = aminoacidsdict
                self.values = [self.id, self.name, self.os, self.ox, self.gn, self.pe, self.sv, self.sequence, self.size, self.aminoacidcounts]
            def __getitem__(self,Index):
                return self.values[Index]

            def __repr__(self):
                return '%s id: %s'%(self.name,self.id)

            def __eq__(self, other):
                return ((self.size) == (other.size))
           
            def __lt__(self, other):
                return ((self.size) < (other.size))
          
            def __gt__(self, other):
               return ((self.size) > (other.size))
           
            def __ne__(self, other):
                return ((self.size) != (other.size))
           
            def __le__(self, other):
                return ((self.size) <= (other.size))
         
            def __ge__(self, other):
                return ((self.size) >= (other.size))

def load_fasta(filename):
    with open(filename) as pr:
        readpr=pr.read()
        sppr2=readpr.split("\n>")
        prlist=[]
        prlen = 0
        prn = len(sppr2)
        iter = 0 
        for i in sppr2:
            strs=i                 # making a string from list
            a=strs.index("|")+1      #search first | sign position
            b=a+6                 # there's the second | sign position
            prid=strs[a:b]       # store the id as string
            c=i.index("OS=")       # search os which is the end of the protein's name
            prname=strs[b+1:c]   # store protein's name
            d=strs.index("OX=")         # store os
            pros=strs[c+3:d]
            e=strs.index("GN=")        # strore ox
            prox=strs[d+3:e]
            f=strs.index("PE=")        # store g
            prgn=strs[e+3:f]
            g=strs.index("SV=")        # store p
            prpe=strs[f+3:g]   
            h=strs.find("\n")       # store sv
            prsv=strs[g+3:h]
            prseq=strs[h:]   #store sequence
            prlist.append(Protein(prid.strip(), prname.strip(), pros.strip(), int(prox.strip()), prgn.strip(), int(prpe.strip()), int(prsv.strip()), prseq.replace('\n','')))
            iter += 1
        return(prlist)

def sort_proteins_pe(proteins):
    sortedpr = sorted(proteins, key = lambda x: x[5], reverse= True)
    return(sortedpr)

def sort_proteins_aa(proteins,aminoacids):
    def calculate(protein, aminoacid):
        lists = []
        ter = 0
        for i in protein:
            lists.append(i)
            ter = lists.count(aminoacid)
        return(ter)
    srtpr = sorted(proteins,key = lambda x: calculate(x[7],aminoacids), reverse = True)
    return(srtpr)

def find_protein_with_motif(proteins, motif):
    good = []
    ind = 0
    ind = len(proteins)
    for i in range(ind):

        a = proteins[i].sequence
        try:
            a.index(motif)
            good.append(proteins[i])
        except(ValueError):
            pass
    return(good)




